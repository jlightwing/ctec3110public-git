<?php

    ini_set('display_errors', 'On');
    ini_set('html_errors', 'On');
    ini_set('xdebug.trace_output_name', 'project.%t');
    xdebug_start_trace();

    include 'project/bootstrap.php';

    xdebug_stop_trace();
?>